use crate::common::ids::*;
use crate::raw::Raw;

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone)]
pub struct Stat(pub Vec<usize>);

#[cfg_attr(debug_assertions, derive(Debug))]
pub struct BaseStat {
    pub pokemon_id: PokemonId,
    pub stat: Stat,
    pub effort_yields: Stat,
}

#[cfg_attr(debug_assertions, derive(Debug))]
pub struct BaseStats(pub Vec<BaseStat>);

impl BaseStats {
    pub fn from_raw(raw: &Raw) -> Self {
        let mut vec = Vec::new();

        if raw.pokemon_stats.0.len() > 0 {
            let mut pokemon_stat_iter = raw.pokemon_stats.0.iter();
            let mut current_base_stat = BaseStat {
                pokemon_id: pokemon_stat_iter.next().unwrap().pokemon_id,
                stat: Stat(Vec::new()),
                effort_yields: Stat(Vec::new()),
            };
            for pokemon_stat in &raw.pokemon_stats.0 {
                if current_base_stat.pokemon_id == pokemon_stat.pokemon_id {
                    debug_assert_eq!(current_base_stat.stat.0.len(), current_base_stat.effort_yields.0.len());
                    let stat_id = pokemon_stat.stat_id.into();
                    if current_base_stat.stat.0.len() >= stat_id {
                        let new_len = stat_id + 1;
                        current_base_stat.stat.0.resize(new_len, 0);
                        current_base_stat.effort_yields.0.resize(new_len, 0);
                    }
                    current_base_stat.stat.0[stat_id] = pokemon_stat.base_stat;
                    current_base_stat.effort_yields.0[stat_id] = pokemon_stat.effort;
                } else {
                    let stat_len = current_base_stat.stat.0.len();
                    vec.push(current_base_stat);
                    current_base_stat = BaseStat {
                        pokemon_id: pokemon_stat.pokemon_id,
                        stat: Stat(Vec::with_capacity(stat_len)),
                        effort_yields: Stat(Vec::with_capacity(stat_len)),
                    };
                    debug_assert_eq!(current_base_stat.stat.0.len(), current_base_stat.effort_yields.0.len());
                    let stat_id = pokemon_stat.stat_id.into();
                    if current_base_stat.stat.0.len() >= stat_id {
                        let new_len = stat_id + 1;
                        current_base_stat.stat.0.resize(new_len, 0);
                        current_base_stat.effort_yields.0.resize(new_len, 0);
                    }
                    current_base_stat.stat.0[stat_id] = pokemon_stat.base_stat;
                    current_base_stat.effort_yields.0[stat_id] = pokemon_stat.effort;
                }
            }
        }
        Self(vec)
    }
}
