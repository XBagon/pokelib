use crate::api::stats::Stat;
use crate::common::ids::*;
use crate::PokeLib;
use crate::raw::natures::Nature;

//TODO: custom build function to check constraints?
#[derive(Builder)]
pub struct PokemonInstance {
    pub pokemon_id: PokemonId,
    pub level: usize,
    pub ivs: Stat,
    pub evs: Stat,
    pub nature_id: NatureId,
}

impl PokemonInstance {
    pub fn stat(&self, lib: &PokeLib) -> Stat {
        let base_stat_index = lib.base_stats.0.binary_search_by_key(&self.pokemon_id, |stat| stat.pokemon_id).unwrap();
        let base_stat = &lib.base_stats.0[base_stat_index];
        let mut stat_vec : Vec<usize> = izip!(base_stat.stat.0.iter(), self.ivs.0.iter(), self.evs.0.iter()).map(|(base, iv, ev)| {
            ((2 * base + iv + (ev/4)) * self.level)/100
        }).collect();

        let mut stat_vec_iter = stat_vec.iter_mut();
        if let Some(first) = stat_vec_iter.next() {
            *first += self.level + 10;
        }
        for stat in stat_vec_iter {
            *stat = *stat + 5;
        }

        let nature_index = lib.raw.natures.0.binary_search_by_key(&self.nature_id, |nature| nature.id).unwrap();
        let Nature { decreased_stat_id, increased_stat_id, .. } = lib.raw.natures.0[nature_index];
        if decreased_stat_id != increased_stat_id {
            let mut index : usize = decreased_stat_id.into();
            stat_vec[index] = (stat_vec[index] as f32 * 0.9) as usize;
            index = increased_stat_id.into();
            stat_vec[index] = (stat_vec[index] as f32 * 1.1) as usize;
        }

        Stat(stat_vec)
    }
}
