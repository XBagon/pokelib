#[macro_use]
extern crate derive_builder;
#[macro_use]
extern crate itertools;

pub mod api;
mod common;
pub mod raw;

use crate::api::stats::BaseStats;
use crate::raw::Raw;
use std::error::Error;
use std::path::Path;

#[cfg_attr(debug_assertions, derive(Debug))]
pub struct PokeLib {
    raw: Raw,
    base_stats: BaseStats,
}

impl PokeLib {
    pub fn parse_from_dir<P: AsRef<Path>>(path: P) -> Result<Self, Box<dyn Error>> {
        let path = &path;
        let raw = Raw::parse_from_dir(path)?;
        Ok(Self {
            base_stats: BaseStats::from_raw(&raw),
            raw,
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::api::pokemon_instance::PokemonInstanceBuilder;
    use crate::api::stats::Stat;
    use crate::PokeLib;

    #[test]
    fn parse_all() {
        let _poke_lib = match PokeLib::parse_from_dir("resources/data") {
            Ok(poke_lib) => poke_lib,
            Err(err) => panic!("{}", err),
        };
    }


    /// https://bulbapedia.bulbagarden.net/wiki/Statistic#Example_2
    #[test]
    fn garchomp_stat() {
        let poke_lib = match PokeLib::parse_from_dir("resources/data") {
            Ok(poke_lib) => poke_lib,
            Err(err) => panic!("{}", err),
        };

        let garchomp = PokemonInstanceBuilder::default()
            .pokemon_id(445.into())
            .level(78)
            .ivs(Stat(vec![24, 12, 30, 16, 23, 5]))
            .evs(Stat(vec![74, 190, 91, 48, 84, 23]))
            .nature_id(11.into())
            .build()
            .unwrap();

        assert_eq!(garchomp.stat(&poke_lib).0,
            vec![
                289,
                278,
                193,
                135,
                171,
                171,
            ]
        );
    }
}
