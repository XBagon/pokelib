pub mod ids;

use serde::de::{self, Deserialize, DeserializeOwned, Deserializer, Unexpected};
use std::error::Error;
use std::path::Path;

pub fn bool_from_int<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    match u8::deserialize(deserializer)? {
        0 => Ok(false),
        1 => Ok(true),
        other => Err(de::Error::invalid_value(
            Unexpected::Unsigned(other as u64),
            &"zero or one",
        )),
    }
}

pub trait ParseVecFrom<T: DeserializeOwned> {
    fn parse_vec_from<P: AsRef<Path>>(path: P) -> Result<Vec<T>, Box<dyn Error>> {
        Ok(csv::Reader::from_path(path)?
            .into_deserialize::<T>()
            .collect::<Result<Vec<T>, _>>()?)
    }
}

impl<T: DeserializeOwned> ParseVecFrom<T> for T {}

#[macro_export]
macro_rules! data {
    ($singular:ident, $plural:ident, $filename:literal) => {
        use crate::common::ParseVecFrom;
        use std::error::Error;
        use std::path::Path;

        #[cfg_attr(debug_assertions, derive(Debug))]
        #[derive(Deserialize, Serialize)]
        pub struct $plural(pub Vec<$singular>);

        impl $plural {
            //TODO: better errors
            pub fn parse_from<P: AsRef<Path>>(path: P) -> Result<Self, Box<dyn Error>> {
                Ok(Self($singular::parse_vec_from(
                    path.as_ref().join($filename),
                )?))
            }
        }
    };
}