macro_rules! Ids {
    ($($ident:ident),*) => {
        $(
            #[cfg_attr(debug_assertions, derive(Debug))]
            #[derive(Clone, Copy, Ord, PartialOrd, Eq, PartialEq, Deserialize, Serialize)]
            pub struct $ident(pub(crate) NonZeroUsize);
            impl fmt::Display for $ident {
                fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    write!(f, "{}", self.0)
                }
            }

            //Probably TEMPORARY
            impl From<usize> for $ident {
                fn from(num: usize) -> Self {
                    Self(NonZeroUsize::new(num).unwrap())
                }
            }


        )*
    };
}

use serde::{Deserialize, Serialize};
use std::fmt;
use std::num::NonZeroUsize;

Ids!(
    BerryId,
    ItemId,
    FirmnessId,
    NaturalGiftTypeId,
    GenerationId,
    DamageClassId,
    SpeciesId,
    AbilityId,
    PokemonId,
    TypeId,
    TargetId,
    EffectId,
    ContestTypeId,
    ContestEffectId,
    SuperContestEffectId,
    TargetTypeId,
    DamageTypeId,
    MoveId,
    LocalLanguageId,
    StatId,
    NatureId,
    FlavorId
);

impl From<StatId> for usize {
    fn from(stat_id: StatId) -> Self {
        stat_id.0.get() - 1
    }
}