use crate::raw::{
    abilities::Abilities, berries::Berries, move_names::MoveNames, move_targets::MoveTargets,
    moves::Moves, natures::Natures, pokemon::Pokemons, pokemon_abilities::PokemonAbilities,
    pokemon_stats::PokemonStats, stats::Stats, type_efficacy::TypeEfficacies, types::Types,
};
use std::error::Error;
use std::path::Path;

pub mod abilities;
//pub mod ability_changelog;
//pub mod ability_changelog_prose;
//pub mod ability_flavor_text;
//pub mod ability_names;
//pub mod ability_prose;
pub mod berries;
//pub mod berry_firmness;
//pub mod berry_firmness_names;
//pub mod berry_flavors;
//pub mod characteristics;
//pub mod characteristic_text;
//pub mod conquest_episodes;
//pub mod conquest_episode_names;
//pub mod conquest_episode_warriors;
//pub mod conquest_kingdoms;
//pub mod conquest_kingdom_names;
//pub mod conquest_max_links;
//pub mod conquest_move_data;
//pub mod conquest_move_displacements;
//pub mod conquest_move_displacement_prose;
//pub mod conquest_move_effects;
//pub mod conquest_move_effect_prose;
//pub mod conquest_move_ranges;
//pub mod conquest_move_range_prose;
//pub mod conquest_pokemon_abilities;
//pub mod conquest_pokemon_evolution;
//pub mod conquest_pokemon_moves;
//pub mod conquest_pokemon_stats;
//pub mod conquest_stats;
//pub mod conquest_stat_names;
//pub mod conquest_transformation_pokemon;
//pub mod conquest_transformation_warriors;
//pub mod conquest_warriors;
//pub mod conquest_warrior_archetypes;
//pub mod conquest_warrior_names;
//pub mod conquest_warrior_ranks;
//pub mod conquest_warrior_rank_stat_map;
//pub mod conquest_warrior_skills;
//pub mod conquest_warrior_skill_names;
//pub mod conquest_warrior_specialties;
//pub mod conquest_warrior_stats;
//pub mod conquest_warrior_stat_names;
//pub mod conquest_warrior_transformation;
//pub mod contest_combos;
//pub mod contest_effects;
//pub mod contest_effect_prose;
//pub mod contest_types;
//pub mod contest_type_names;
//pub mod egg_groups;
//pub mod egg_group_prose;
//pub mod encounters;
//pub mod encounter_conditions;
//pub mod encounter_condition_prose;
//pub mod encounter_condition_values;
//pub mod encounter_condition_value_map;
//pub mod encounter_condition_value_prose;
//pub mod encounter_methods;
//pub mod encounter_method_prose;
//pub mod encounter_slots;
//pub mod evolution_chains;
//pub mod evolution_triggers;
//pub mod evolution_trigger_prose;
//pub mod experience;
//pub mod genders;
//pub mod generations;
//pub mod generation_names;
//pub mod growth_rates;
//pub mod growth_rate_prose;
//pub mod items;
//pub mod item_categories;
//pub mod item_category_prose;
//pub mod item_flags;
//pub mod item_flag_map;
//pub mod item_flag_prose;
//pub mod item_flavor_summaries;
//pub mod item_flavor_text;
//pub mod item_fling_effects;
//pub mod item_fling_effect_prose;
//pub mod item_game_indices;
//pub mod item_names;
//pub mod item_pockets;
//pub mod item_pocket_names;
//pub mod item_prose;
//pub mod languages;
//pub mod language_names;
//pub mod locations;
//pub mod location_areas;
//pub mod location_area_encounter_rates;
//pub mod location_area_prose;
//pub mod location_game_indices;
//pub mod location_names;
//pub mod machines;
pub mod moves;
//pub mod move_battle_styles;
//pub mod move_battle_style_prose;
//pub mod move_changelog;
//pub mod move_damage_classes;
//pub mod move_damage_class_prose;
//pub mod move_effects;
//pub mod move_effect_changelog;
//pub mod move_effect_changelog_prose;
//pub mod move_effect_prose;
//pub mod move_flags;
//pub mod move_flag_map;
//pub mod move_flag_prose;
//pub mod move_flavor_summaries;
//pub mod move_flavor_text;
//pub mod move_meta;
//pub mod move_meta_ailments;
//pub mod move_meta_ailment_names;
//pub mod move_meta_categories;
//pub mod move_meta_category_prose;
//pub mod move_meta_stat_changes;
pub mod move_names;
pub mod move_targets;
//pub mod move_target_prose;
pub mod natures;
//pub mod nature_battle_style_preferences;
//pub mod nature_names;
//pub mod nature_pokeathlon_stats;
//pub mod pal_park;
//pub mod pal_park_areas;
//pub mod pal_park_area_names;
//pub mod pokeathlon_stats;
//pub mod pokeathlon_stat_names;
//pub mod pokedexes;
//pub mod pokedex_prose;
//pub mod pokedex_version_groups;
pub mod pokemon;
pub mod pokemon_abilities;
//pub mod pokemon_colors;
//pub mod pokemon_color_names;
//pub mod pokemon_dex_numbers;
//pub mod pokemon_egg_groups;
//pub mod pokemon_evolution;
//pub mod pokemon_forms;
//pub mod pokemon_form_generations;
//pub mod pokemon_form_names;
//pub mod pokemon_form_pokeathlon_stats;
//pub mod pokemon_game_indices;
//pub mod pokemon_habitats;
//pub mod pokemon_habitat_names;
//pub mod pokemon_items;
//pub mod pokemon_moves;
//pub mod pokemon_move_methods;
//pub mod pokemon_move_method_prose;
//pub mod pokemon_shapes;
//pub mod pokemon_shape_prose;
//pub mod pokemon_species;
//pub mod pokemon_species_flavor_summaries;
//pub mod pokemon_species_flavor_text;
//pub mod pokemon_species_names;
//pub mod pokemon_species_prose;
pub mod pokemon_stats;
//pub mod pokemon_types;
//pub mod regions;
//pub mod region_names;
pub mod stats;
//pub mod stat_names;
//pub mod super_contest_combos;
//pub mod super_contest_effects;
//pub mod super_contest_effect_prose;
pub mod type_efficacy;
pub mod types;
//pub mod type_game_indices;
//pub mod type_names;
//pub mod versions;
//pub mod version_groups;
//pub mod version_group_pokemon_move_methods;
//pub mod version_group_regions;
//pub mod version_names;

#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Raw {
    pub types: Types,
    pub pokemons: Pokemons,
    pub abilities: Abilities,
    pub berries: Berries,
    pub type_efficacies: TypeEfficacies,
    pub pokemon_abilities: PokemonAbilities,
    pub moves: Moves,
    pub move_names: MoveNames,
    pub move_targets: MoveTargets,
    pub natures: Natures,
    pub pokemon_stats: PokemonStats,
    pub stats: Stats,
}

impl Raw {
    pub fn parse_from_dir<P: AsRef<Path>>(path: P) -> Result<Self, Box<dyn Error>> {
        let path = &path;
        Ok(Self {
            types: Types::parse_from(path)?,
            pokemons: Pokemons::parse_from(path)?,
            abilities: Abilities::parse_from(path)?,
            berries: Berries::parse_from(path)?,
            type_efficacies: TypeEfficacies::parse_from(path)?,
            pokemon_abilities: PokemonAbilities::parse_from(path)?,
            moves: Moves::parse_from(path)?,
            move_names: MoveNames::parse_from(path)?,
            move_targets: MoveTargets::parse_from(path)?,
            natures: Natures::parse_from(path)?,
            pokemon_stats: PokemonStats::parse_from(path)?,
            stats: Stats::parse_from(path)?,
        })
    }
}
