use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct MoveName {
    pub move_id: MoveId,
    pub local_language_id: LocalLanguageId,
    pub name: String,
}

data!(MoveName, MoveNames, "move_names.csv");
