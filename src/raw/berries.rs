use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct Berry {
    pub id: BerryId,
    pub item_id: ItemId,
    pub firmness_id: FirmnessId,
    pub natural_gift_power: usize,
    pub natural_gift_type_id: NaturalGiftTypeId,
    pub size: usize,
    pub max_harvest: usize,
    pub growth_time: usize,
    pub soil_dryness: usize,
    pub smoothness: usize,
}

data!(Berry, Berries, "berries.csv");
