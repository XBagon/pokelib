use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct Move {
    pub id: MoveId,
    pub identifier: String,
    pub generation_id: GenerationId,
    pub type_id: TypeId,
    pub power: Option<usize>,
    pub pp: Option<usize>,
    pub accuracy: Option<usize>,
    pub priority: isize,
    pub target_id: TargetId,
    pub damage_class_id: DamageClassId,
    pub effect_id: EffectId,
    pub effect_chance: Option<usize>,
    pub contest_type_id: Option<ContestTypeId>,
    pub contest_effect_id: Option<ContestEffectId>,
    pub super_contest_effect_id: Option<SuperContestEffectId>,
}

data!(Move, Moves, "moves.csv");
