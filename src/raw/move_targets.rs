use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct MoveTarget {
    pub id: TargetId,
    pub identifier: String,
}

data!(MoveTarget, MoveTargets, "move_targets.csv");
