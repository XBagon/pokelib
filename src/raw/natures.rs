use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct Nature {
    pub id: NatureId,
    pub identifier: String,
    pub decreased_stat_id: StatId,
    pub increased_stat_id: StatId,
    pub hates_flavor_id: FlavorId,
    pub likes_flavor_id: FlavorId,
    pub game_index: usize,
}

data!(Nature, Natures, "natures.csv");
