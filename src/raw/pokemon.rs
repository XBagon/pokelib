use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct Pokemon {
    pub id: PokemonId,
    pub identifier: String,
    pub species_id: SpeciesId,
    pub height: usize,
    pub weight: usize,
    pub base_experience: usize,
    pub order: usize,
    #[serde(deserialize_with = "crate::common::bool_from_int")]
    pub is_default: bool,
}

data!(Pokemon, Pokemons, "pokemon.csv");
