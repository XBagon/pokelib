use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct PokemonStat {
    pub pokemon_id: PokemonId,
    pub stat_id: StatId,
    pub base_stat: usize,
    pub effort: usize,
}

data!(PokemonStat, PokemonStats, "pokemon_stats.csv");
