use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct Type {
    pub id: TypeId,
    pub identifier: String,
    pub generation_id: GenerationId,
    pub damage_class_id: Option<DamageClassId>,
}

data!(Type, Types, "types.csv");
