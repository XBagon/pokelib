use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct TypeEfficacy {
    pub damage_type_id: DamageTypeId,
    pub target_type_id: TargetTypeId,
    pub damage_factor: usize,
}

data!(TypeEfficacy, TypeEfficacies, "type_efficacy.csv");
