use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct Ability {
    pub id: AbilityId,
    pub identifier: String,
    pub generation_id: GenerationId,
    #[serde(deserialize_with = "crate::common::bool_from_int")]
    pub is_main_series: bool,
}

data!(Ability, Abilities, "abilities.csv");
