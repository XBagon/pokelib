use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};
use std::num::NonZeroUsize;

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct Stat {
    pub id: StatId,
    pub damage_class_id: Option<DamageClassId>,
    pub identifier: String,
    #[serde(deserialize_with = "crate::common::bool_from_int")]
    pub is_battle_only: bool,
    pub game_index: Option<NonZeroUsize>,
}

data!(Stat, Stats, "stats.csv");
