use crate::common::ids::*;
use crate::data;
use serde::{Deserialize, Serialize};
use std::num::NonZeroUsize;

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Clone, Deserialize, Serialize)]
pub struct PokemonAbility {
    pub pokemon_id: PokemonId,
    pub ability_id: AbilityId,
    pub is_hidden: usize,
    pub slot: NonZeroUsize,
}

data!(PokemonAbility, PokemonAbilities, "pokemon_abilities.csv");
