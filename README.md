The data in [resources](resources) is copied from https://github.com/veekun/pokedex/blob/master/pokedex/data/csv.

At the moment this library can only work correctly when data is already sorted by ids.